package com.pongpipat;
import static org.junit.Assert.assertArrayEquals;

import com.pongpipat.week5.BubblesortApp;
import static org.junit.Assert.assertArrayEquals;


import org.junit.Test;
public class BubblesortAppTest {
    @Test
    public void shouldBubbleTestcase1(){
        int [] arr = {5,4,3,2,1};
        int [] exp = {4,3,2,1,5};
        int first = 0;
        int second = 4;
        BubblesortApp.bubble(arr,first,second);
        assertArrayEquals(exp, arr);
    }
    @Test
    public void shouldBubbleTestcase2(){
        int [] arr = {4,3,2,1,5};
        int [] exp = {3,2,1,4,5};
        int first = 0;
        int second = 3;
        BubblesortApp.bubble(arr,first,second);
        assertArrayEquals(exp, arr);
    }
    @Test
    public void shouldBubbleTestcase3(){
        int [] arr = {3,2,1,4,5};
        int [] exp = {2,1,3,4,5};
        int first = 0;
        int second = 2;
        BubblesortApp.bubble(arr,first,second);
        assertArrayEquals(exp, arr);
    }
    @Test
    public void shouldBubbleTestcase4(){
        int [] arr ={2,1,3,4,5};
        int [] exp = {1,2,3,4,5,};
        int first = 0;
        int second = 1;
        BubblesortApp.bubble(arr,first,second);
        assertArrayEquals(exp, arr);
    }
    @Test
    public void shouldBubbleSortTestCase1(){
        int [] arr = {5,4,3,2,1};
        int [] exp = {1,2,3,4,5};
        BubblesortApp.bubblesort(arr);
        assertArrayEquals(exp, arr);
    }
    @Test
    public void shouldBubbleSortTestCase2(){
        int [] arr = {10,9,8,7,6,5,4,3,2,1};
        int [] sortArr = {1,2,3,4,5,6,7,8,9,10};
        BubblesortApp.bubblesort(arr);
        assertArrayEquals(sortArr, arr);
    }
    @Test
    public void shouldBubbleSortTestCase3(){
        int [] arr = {6,9,3,7,10,5,4,8,2,1};
        int [] sortArr = {1,2,3,4,5,6,7,8,9,10};
        BubblesortApp.bubblesort(arr);
        assertArrayEquals(sortArr, arr);
    }
}
