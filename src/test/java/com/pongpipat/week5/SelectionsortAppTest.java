package com.pongpipat.week5;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class SelectionsortAppTest {
    @Test
    public void shouldFineMinIndexTestcase1(){
        int [] arr = {5,4,3,2,1};
        int pos = 0;
        int min = SelectionSortApp.fineMinIndex(arr,pos);
        assertEquals(4,min);
    } 
    @Test
    public void shouldFineMinIndexTestcase2(){
        int [] arr = {1,4,3,2,5};
        int pos = 1;
        int min = SelectionSortApp.fineMinIndex(arr,pos);
        assertEquals(3,min);
    }
    @Test
    public void shouldFineMinIndexTestcase3(){
        int [] arr = {1,2,3,4,5};
        int pos = 2;
        int min = SelectionSortApp.fineMinIndex(arr,pos);
        assertEquals(2,min);
    }
    @Test
    public void shouldFineMinIndexTestcase4(){
        int [] arr = {1,1,1,1,1,0,1};
        int pos = 0;
        int min = SelectionSortApp.fineMinIndex(arr,pos);
        assertEquals(5,min);
    }
    @Test
    public void shouldSwapTestCase1(){
        int [] arr = {5,4,3,2,1};
        int [] exp = {1,4,3,2,5};
        int first = 0;
        int second = 4;
        SelectionSortApp.Swap(arr,first,second);
        assertArrayEquals(exp,arr);
    }
    @Test
    public void shouldSelectionSortTestCase1(){
        int [] arr = {5,4,3,2,1};
        int [] sortArr = {1,2,3,4,5,};
        SelectionSortApp.selectionsort(arr);
        assertArrayEquals(sortArr, arr);
    }
    @Test
    public void shouldSelectionSortTestCase2(){
        int [] arr = {10,9,8,7,6,5,4,3,2,1};
        int [] sortArr = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.selectionsort(arr);
        assertArrayEquals(sortArr, arr);
    }
    @Test
    public void shouldSelectionSortTestCase3(){
        int [] arr = {6,9,3,7,10,5,4,8,2,1};
        int [] sortArr = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.selectionsort(arr);
        assertArrayEquals(sortArr, arr);
    }
}
