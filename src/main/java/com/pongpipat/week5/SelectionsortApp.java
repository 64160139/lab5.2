package com.pongpipat.week5;


public class SelectionSortApp {
    public static void main(String[] args) {
        int [] arr = new int[1000];
        randomArr(arr);
        print(arr);
        selectionsort(arr);
        print(arr);
    }

    public static void print(int [] arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
    public static void randomArr(int [] arr ){
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random()*10000);
        }
    }
    public static int  fineMinIndex(int [] arr,int pos){
        int min = pos;
        for (int i = pos+1; i < arr.length; i++) {
            if(arr[min] > arr[i]){
                min = i;
            }
        }
        return min;
    }

    public static void Swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public static void selectionsort(int[] arr) {
        int min ;
        for(int pos = 0; pos < arr.length -1 ;pos++){
            min = fineMinIndex(arr,pos);
            Swap(arr,min,pos);
        }      
    }
}
